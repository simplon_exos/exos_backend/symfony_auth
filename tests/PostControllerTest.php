<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

class PostControllerTest extends WebTestCase
{

    public function testShowAllArticles()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Blog');
        
        $this->assertSelectorExists('.post');

        $this->assertEquals('16', $crawler->filter('.post')->count());
        $this->assertSelectorTextContains('.post', 'Title 1');
    }

    public function testShowUserPosts()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'mail1@mail.com',
            'PHP_AUTH_PW'   => '1234',
        ]);
        $crawler = $client->request('GET', '/user/post');

        $this->assertResponseIsSuccessful();

        $this->assertSelectorExists('.post');
        $this->assertCount(4, $crawler->filter('.post'));
        
        $this->assertSelectorTextContains('.author', 'mail1@mail.com');
        $this->assertSelectorTextNotContains('.author', 'mail2@mail.com');


    }
}
